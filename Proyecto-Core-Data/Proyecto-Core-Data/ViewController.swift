//
//  ViewController.swift
//  Proyecto-Core-Data
//
//  Created by Jose Gonzalez on 24/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import UIKit

import CoreData

class ViewController: UIViewController {

    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var addressTextField: UITextField!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    var singlePerson:Person?
    
    var allPerson:[Person]!
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate ).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    func savePerson() {
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        
        person.address = addressTextField.text ?? ""
        person.name = nameTextField.text ?? ""
        person.phone = phoneTextField.text ?? ""
        
        do {
            try manageObjectContext.save()
            
          clearFields()
            
        }catch{
            
            print("Error")
        }
        
    }
    
    func clearFields(){
        
        addressTextField.text = ""
        nameTextField.text = ""
        phoneTextField.text = ""
    }
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        savePerson()
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        
        
        
        if nameTextField.text == "" {
            
               findAll()
        }
        else{
            
            findOne()
        }
        
        
        
     
    }
    
    func findAll(){
        
        //let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do{
            var results = try (manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            allPerson = results as! [Person]
            performSegue(withIdentifier: "findAllSegue", sender: self)
            
            print()
            print()
            
            /*
            for result in results {
                
                let person = result as! Person
                
                print("Name: \(person.name ?? "") ", terminator: "")
                 print("Address: \(person.address ?? "") " , terminator: "")
                 print("Phone: \(person.phone ?? "") " , terminator: "")
                
                print()
                print()
            }*/
            
        }catch{
            
            print("Error Finding")
        }
        
    }
    
    func findOne(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        request.entity = entityDescription
        
        
        let predicate = NSPredicate(format: "name= %@", nameTextField.text!)
        
        request.predicate = predicate
     
        do{
            let results = try (manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            if results.count > 0 {
   
                 let match = results[0] as! Person
                singlePerson = match
                performSegue(withIdentifier: "findOneSegue", sender: self)
                
               
                //addressTextField.text = match.address
                //nameTextField.text = match.name
                //phoneTextField.text = match.phone
                
            }else{
                
    
                addressTextField.text = "n/a"
                nameTextField.text = "n/a"
                phoneTextField.text = "n/a"
                
                
            }
            
        }catch{
            
            print("Error Finding One")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "findOneSegue" {
            
            let destination = segue.destination as! FindOneViewController
            destination.person = singlePerson
            
            
        }
        if segue.identifier == "findAllSegue" {
            
            let destination = segue.destination as! FindAllViewController
            destination.person1 = allPerson
            
            
        }
    }
    
}
























