//
//  FindAllViewController.swift
//  Proyecto-Core-Data
//
//  Created by Jose Gonzalez on 30/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import UIKit

class FindAllViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    

    
 
    
    var person1:[Person]!
    
    var names:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        for result in person1 {
            
             print("")
            let person = result as! Person
            
            print("Name: \(person.name ?? "") ", terminator: "")
            print("Address: \(person.address ?? "") " , terminator: "")
            print("Phone: \(person.phone ?? "") " , terminator: "")
            
            print()
            print()
            
            names.append(person.name ?? "")
            print(names)
        }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return person1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "mycell")
        cell.textLabel?.text  = person1[indexPath.row].name
        
        return cell
    }
    
    
    
    
    
    
    

}
